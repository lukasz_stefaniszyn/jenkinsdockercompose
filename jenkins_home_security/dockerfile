FROM alpine
MAINTAINER Lukasz Stefaniszyn <stefaniszynlukasz@gmail.com>

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000

ENV JENKINS_HOME /var/jenkins_home

# Jenkins is run with user `jenkins`, uid = 1000
# If you bind mount a volume from the host or a data container, 
# ensure you use the same uid
RUN addgroup -S ${group} -g ${gid} && adduser -S -g ${group} -u ${gid} ${user} 


COPY jenkins_home $JENKINS_HOME

RUN chown -R 1000:1000 -R $JENKINS_HOME
RUN find $JENKINS_HOME -type d -exec chmod 755 {} \;
RUN find $JENKINS_HOME -type f -exec chmod 644 {} \;

VOLUME $JENKINS_HOME/users
VOLUME $JENKINS_HOME/userContent
VOLUME $JENKINS_HOME/secrets
VOLUME $JENKINS_HOME/nodes
VOLUME $JENKINS_HOME/vc_jenkins_security

USER ${user}

ENTRYPOINT exec top -b
