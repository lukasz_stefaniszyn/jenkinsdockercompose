#!/usr/bin/perl
use XML::LibXML;

print "\nargument 1: $ARGV[0]\n";
print "argument 2: $ARGV[1]\n";
print "argument 3: $ARGV[2]\n";

my $template = "$ARGV[2]";
my $parser = XML::LibXML->new();
my $doc = $parser->parse_file($template);

my($object) = $doc->findnodes("$ARGV[1]/text()");
my $text = XML::LibXML::Text->new("$ARGV[0]");
$object->replaceNode($text);

open (my $Output, ">$template") or die "Could not write $template";
print $Output $doc->toString;
close ($Output) or die "Could not close generated $template ";
