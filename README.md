# Jenkins book

https://bitbucket.org/lukasz_stefaniszyn/jenkinsdockercompose/downloads/jenkins_thedefinitiveguide.pdf

# Jenkins sample images
* https://bitbucket.org/lukasz_stefaniszyn/jenkinsdockercompose/downloads/jenkins_home_jobs.tar.gz
* https://bitbucket.org/lukasz_stefaniszyn/jenkinsdockercompose/downloads/jenkins_home_security.tar.gz
* https://bitbucket.org/lukasz_stefaniszyn/jenkinsdockercompose/downloads/jenkins_home_cop.zip


# Install dokcer service and create users

## Install docker service
> curl -fsSL get.docker.com -o get-docker.sh

> sh get-docker.sh

## Create users

> sudo useradd -M jenkins

> sudo usermod -L jenkins

> sudo usermod -a -G docker jenkins


# I solution  - mount local folders to volume containers

## Create volume and mount jenkins_home folder to data container
> docker run -d --name vc_jenkins_mng --volume /home/lstefan/jenkins_home:/var/jenkins_home alpine true

> docker inspect -f '{{ .Mounts }}' vc_jenkins_mng


## Create volume and mount jenkins_home/jobs and jenkins_home/workspace folder to data container
> docker run -d --volume /home/lstefan/jenkins_home_jobs/jobs:/var/jenkins_home/jobs --volume /home/lstefan/jenkins_home_jobs/workspace:/var/jenkins_home/workspace --name vc_jenkins_jobs alpine true

> docker inspect -f '{{ .Mounts }}' vc_jenkins_jobs

## Run Jenkins and blend Data Containers
> docker run -it --rm --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_mng --name jenkins jenkins:2.60.1 /bin/bash

> docker run -d --rm --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_mng --name jenkins -p 8080:8080 -p 50000:50000 --env JAVA_OPTS="-Xmx8192m" jenkins:2.60.1

> docker inspect -f '{{ .Mounts }}' jenkins


# II solution  - create image with data

## Create volume and mount jenkins_home folder to data container

dockerfile:
```
FROM alpine
MAINTAINER Lukasz Stefaniszyn <stefaniszynlukasz@gmail.com>
COPY jenkins_home.tar.gz /var/jenkins_home.tar.gz
RUN tar -zxf var/jenkins_home.tar.gz -C var
RUN rm -f jenkins_home.tar.gz
VOLUME /var/jenkins_home
ENTRYPOINT exec top -b
```

> docker build -t vc_jenkins_mng .

> docker run -d --name vc_jenkins_mng vc_jenkins_mng

> docker inspect vc_jenkins_mng


## Create volume and mount jenkins_home/jobs and jenkins_home/workspace folder to data container
> docker run -d --volume /home/lstefan/jenkins_home_jobs/jobs:/var/jenkins_home/jobs --volume /home/lstefan/jenkins_home_jobs/workspace:/var/jenkins_home/workspace --name vc_jenkins_jobs alpine true

> docker inspect vc_jenkins_jobs

## Run Jenkins and blend Data Containers
> docker run -it --rm --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_mng --name jenkins jenkins:2.60.1 /bin/bash

> docker run -d --rm --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_mng --volumes-from vc_jenkins_security -p 8080:8080 --env JAVA_OPTS="-Xmx8192m -e "PATH=$PATH:/var/jenkins_home/opt/apache-maven-3.3.9/bin:/var/jenkins_home/opt/apache-ant-1.10.1/bin" --name jenkins jenkins/jenkins:2.73.1


# Clean up images, volumes, etc
[https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes]

**Remove dangling images**
> docker rmi $(docker images -f dangling=true -q)

**Remove all images**
> docker rmi $(docker images -a -q)

**Removing images according to a pattern**
> docker images | grep "pattern" | awk '{print $2}' | xargs docker rm

**Remove all exited containers**
> docker rm $(docker ps -a -f status=exited -q)

**Remove containers according to a pattern**
> docker ps -a | grep "pattern" | awk '{print $1}' | xargs docker rmi

**Remove dangling volumes**
> docker volume rm $(docker volume ls -f dangling=true -q)


#TO READ

Archive and restore Data volume

+ [https://dzone.com/articles/docker-backup-your-data-volumes-to-docker-hub]

+ [https://serverfault.com/questions/632122/how-do-i-deploy-a-docker-container-and-associated-data-container-including-cont?answertab=votes#tab-top]

+ [https://www.digitalocean.com/community/tutorials/how-to-share-data-between-docker-containers]

Jenkins Docker in docker 
https://renzedevries.wordpress.com/2016/06/30/building-containers-with-docker-in-docker-and-jenkins/

http://container-solutions.com/continuous-delivery-with-docker-on-mesos-in-less-than-a-minute/

http://nemerosa.ghost.io/2016/09/27/docker-in-docker-in-jenkins-pipeline/

http://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/

http://container-solutions.com/running-docker-in-jenkins-in-docker/



chmod 777 -R jenkins_home/
docker run -d --rm --name jenkins -v jenkins_home/:/var/jenkins_home/ -p 8080:8080 -p 50000:50000 --env JAVA_OPTS="-Xmx8192m jenkins/jenkins:2.73.1

docker run -it --rm --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_mng --volumes-from vc_jenkins_security -p 8080:8080 --env JAVA_OPTS="-Xmx8192m -e "PATH=$PATH:/var/jenkins_home/opt/apache-maven-3.3.9/bin:/var/jenkins_home/opt/apache-ant-1.10.1/bin" --name jenkins jenkins/jenkins:2.73.1


##Jenkins with Docker## 

> sudo apt-get upgrade -y

> sudo apt-get install -y sudo libltdl-dev

> GID=$(cut -d: -f3 < <(getent group docker))

> docker run -it --rm -u jenkins:${GID}  --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_mng --volumes-from vc_jenkins_security -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker -p 8080:8080 -e "PATH=$PATH:/var/jenkins_home/opt/apache-maven-3.3.9/bin:/var/jenkins_home/opt/apache-ant-1.10.1/bin" -v /usr/lib/x86_64-linux-gnu/libltdl.so.7:/usr/lib/x86_64-linux-gnu/libltdl.so.7 --env JAVA_OPTS="-Xmx8192m" --name jenkins jenkins/jenkins:2.73.1


## Jenkins slave with docker ##

https://developer.jboss.org/people/pgier/blog/2014/06/30/on-demand-jenkins-slaves-using-docker?_sscc=t

http://techbus.safaribooksonline.com/9781787125230/397fe4f1_e549_4e87_ac17_05e732970088_xhtml?percentage=0&reader=html#X2ludGVybmFsX0h0bWxWaWV3P3htbGlkPTk3ODE3ODcxMjUyMzAlMkYyYjQyODRlYV80ZWYzXzRjZDZfYTI0MF9jZDJhNWVmZDY3YTdfeGh0bWwmcXVlcnk9

## Docker remote API enable ##

Allow port communication:

> sudo ufw allow 4243/tcp 

> sudo ufw allow 32000:33000/tcp

Enable Docker API:

> sudo vi /lib/systemd/system/docker.service
> ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock
 
> sudo systemctl daemon-reload

> sudo service docker restart

Test

> curl --noproxy GET http://127.0.0.1:4243/version








