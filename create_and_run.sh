echo "Set global variables"
REPO_HOME=/home/lstefan/repo/jenkinsdockercompose/
JOBS_HOME=/var/jenkins_home/jobs
USERS_HOME=/var/jenkins_home/users

echo "Remove older docker images"
docker rm -f jenkins
docker rm -f vc_jenkins_security
docker rm -f vc_jenkins_mng
docker rm -f vc_jenkins_jobs

echo "Building jenkins_home_security image"
cd $REPO_HOME/jenkins_home_security/
docker build --tag vc_jenkins_security .
docker run -d --restart on-failure --name vc_jenkins_security vc_jenkins_security

echo "Building jenkins_home_jobs image"
cd $REPO_HOME/jenkins_home_jobs/
docker build -t vc_jenkins_jobs .
docker run -d --restart on-failure --name vc_jenkins_jobs vc_jenkins_jobs

echo "Set default branch to Training selenium_workshop job"
docker exec -w $JOBS_HOME vc_jenkins_jobs sh -c "./nodes.pl develop /project/scm/branches/hudson.plugins.git.BranchSpec/name Training/jobs/selenium_workshop/config.xml"

#echo "Set api Token to user - user1"
#docker exec -w $USERS_HOME vc_jenkins_security sh -c "./nodes.pl yourApiToken /user/properties/jenkins.security.ApiTokenProperty/apiToken user1/config.xml"

echo "Building jenkins_home_mng image"
cd $REPO_HOME/jenkins_home_mng/
docker build -t vc_jenkins_mng .
docker run -d --restart on-failure --name vc_jenkins_mng vc_jenkins_mng

echo "Building jenkins_slave image"
cd $REPO_HOME/jenkins_slave/
docker build -t lucst/jenkins_slave:latest -t lucst/jenkins_slave:1.4 .

echo "Start Jenkins"
docker ps
docker run -d --rm --volumes-from vc_jenkins_jobs --volumes-from vc_jenkins_security --volumes-from vc_jenkins_mng -p 8080:8080  --env JAVA_OPTS="-Xmx8192m"  --name jenkins jenkins/jenkins:2.73.1
